import numpy as np

# Creates a foldable offset dish. Not quite working yet

num_slices = 16
def coords(theta, r):
    '''
        calculates the intersection points of a line with function y = tan(theta)x with the circle of function (y+r)**2 + (x+r)**2 = r**2.
        returns two points, each containing an x,y, and distance to the origin.
        This distance to the origin is used as the x value for the length of the parabola.
    '''
    t_a = np.tan(theta)
    x_a = (2*r*t_a + 2*r)
    x_disc = np.sqrt((-2*r*t_a-2*r)**2 - (4*(r**2))*(t_a**2 + 1))
    x_bot = 2 * (t_a**2 + 1)
    x1 = (x_a + x_disc) / x_bot # this is the outside one
    x2 = (x_a - x_disc) / x_bot # inside
    y1 = t_a * x1
    y2 = t_a * x2
    d1 = np.sqrt(x1**2 + y1**2) # distances from 0,0
    d2 = np.sqrt(x2**2 + y2**2)

    return [(x1, y1, d1), (x2, y2, d2)] # furthest from the origin, then closest.

def test_coords(): # tests the coords function by plotting lines and circles and stuff
    import matplotlib.pyplot as plt
    import time

    n = 10

    theta = np.linspace(0,np.pi/2, n)

    start = time.time()

    p1, p2 = coords(theta, 2)

    total = time.time() - start
    total *= 1000 # ms

    print("Time taken to run through {} intersection points: {:010.6f}ms".format(n, total))
    x1,y1,_ = p1
    x2,y2,_ = p2
    plt.plot([x1,x2], [y1,y2],'bo-') # intersection points

    circle1 = plt.Circle((2,2),2,color='r')
    plt.gcf().gca().add_artist(circle1)
    plt.gcf().gca().set_aspect(1)

    plt.grid()
    plt.xlim([-1,5])
    plt.ylim([-1,5])
    plt.show()

a = 1/4
b = 0

def f(x): # function for the parabola
    return a*(x**2) + b*x

def ds(x, dx): # returns the length of a small section of the curve
    x1 = x - (dx/2)
    x2 = x + (dx/2)
    dy = f(x1) - f(x2)
    ds = (dx**2 + dy**2)**0.5
    return ds

def s(x1, x2): # returns the total length of the curve using ds() with FEM
    total = 0
    xnums, dx = np.linspace(x1, x2, 1000, retstep=True)
    for x in xnums:
        total += ds(x, dx)
    return total

def rotate(x1, y1, angle): # rotates x1, y1 by angle radians
    rot_x = (x1 * np.cos(angle)) - (y1 * np.sin(angle))
    rot_y = (y1 * np.cos(angle)) + (x1 * np.sin(angle))
    return rot_x, rot_y


def get_slice(theta, r): # gets the slice of the parabola given an angle and the radius of the circle projected onto the xy plane.
    # get coords for theta
    p1, p2 = coords(theta, r)
    _,_,d1 = p1 # distances from centre
    _,_,d2 = p2

    # generate x values inside the circle
    n_step = 50
    x_vals = np.linspace(0, d1, n_step) # generate n_step evenly spaced x values that are inside the circle
    length_vals = []
    for x in x_vals:
        length = s(0, x) # calculate length up to that x value
        length_vals.append(length)

    return x_vals, np.array(length_vals)

def w(theta, r): # calculates arc length with angle and radius
    return theta * r

if __name__ == "__main__": # probably not necessary
    import matplotlib.pyplot as plt

    fig = plt.figure(figsize=[6,6])
    ax = fig.add_subplot(111)

    n_slices = 10 # number of slices in the first half of the first quadrant, so between 0 and pi/4
                  # afterwards it is mirrored around the y = x line to give the full circle
    radius = 2    # radius of offset reflector

    theta_vals, d_theta = np.linspace(0, np.pi/4, n_slices, retstep=True) # generate the range of theta values to run through

    for idx, theta in enumerate(theta_vals):
        print(theta)

        x_vals, s_vals = get_slice(theta, radius) # get the length of the slice in the circle

        w_vals = w(d_theta, x_vals) # generate the width values for this specific slice

        plot_theta = theta - np.pi/4 # move everything so that the centre of the circle is at y = 0

        # rotate
        rot_svals, rot_wvals = rotate(s_vals, w_vals/2, plot_theta)
        # plot both sides of the slice.
        ax.plot(rot_svals, rot_wvals, "k-", linewidth=0.5)
        ax.plot(rot_svals, -rot_wvals, "k-", linewidth=0.5)



    ax.grid()
    ax.set_aspect(aspect=1)

    plt.show() # comment this out when you're exporting as an svg

    # uncomment this when you're exporting as an svg.
    #plt.axis('off') # turns off superfluous stuff not 100% sure to be honest
    #plt.gca().set_position([0, 0, 1, 1]) # sets axis position

    #plt.savefig("offset.svg")
