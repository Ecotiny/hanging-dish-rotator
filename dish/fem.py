import numpy as np
import matplotlib.pyplot as plt

# creates a foldable dish using FEM of the length of a curve (hence fem.py),
# rather than analytically solving for the length.

a = 1/8

def f(x):
    return a*(x**2)

def ds(x, dx):
    x1 = x - (dx/2)
    x2 = x + (dx/2)
    dy = f(x1) - f(x2)
    ds = (dx**2 + dy**2)**0.5
    return ds

def s(x1, x2):
    total = 0
    xnums, dx = np.linspace(x1, x2, 1000, retstep=True)
    for x in xnums:
        total += ds(x, dx)
    return total

def w(theta, x):
    return theta * x

def rotate_x(x1, y1, angle):
    return (x1 * np.cos(angle)) - (y1 * np.sin(angle))

def rotate_y(x1, y1, angle):
    return (y1 * np.cos(angle)) + (x1 * np.sin(angle))

dx = 0.1
radius = 4
x_vals = np.arange(0, radius+dx, dx)

n_slice = 16

#plt.plot(x_vals, ds(x_vals, dx))
s_y = []
for x in x_vals:
    s_y.append(s(0, x))

s_y = np.array(s_y)

fig = plt.figure(figsize=[6,6])
ax = fig.add_subplot(111)

d_theta = (2*np.pi)/n_slice
w_vals = w(d_theta, x_vals)

for sl in range(n_slice):
    angle = -sl*((2*np.pi)/n_slice)
    # rotate w_vals and s_y
    rot_sy = rotate_x(s_y, w_vals/2, angle)
    rot_wvals = rotate_y(s_y, w_vals/2, angle)
    # plot
    ax.plot(rot_sy, rot_wvals, "k-")
    ax.plot(rot_sy, -rot_wvals, "k-")
    #ax.plot(x_vals, x_vals/2)
    #ax.plot(x_vals, -x_vals/2)

ax.grid()
ax.set_aspect(aspect=1)

#d_theta = (2*np.pi)/n_slice
#for sl in range(n_slice):
   #theta_vals = np.array([sl*d_theta]*len(x_vals))
   ## baseline
   ##plt.polar(theta_vals, s_y)
   #plt.plot(theta_vals, s_y)
   ## offset of baseline
   #print(w(sl*d_theta, s_y))
##   plt.polar(theta_vals, x_vals)

plt.axis('off')
plt.gca().set_position([0, 0, 1, 1])

plt.savefig("out.svg")
plt.show()
