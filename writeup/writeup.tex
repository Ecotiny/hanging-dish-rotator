\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{fullpage}
% \usepackage[nomarkers,figuresonly]{endfloat}

% Title Page
\title{Pulling the Right Strings: Tracking objects in space using highly-strung dishes}
\author{Linus Molteno}


\begin{document}
\maketitle

\begin{abstract}
I describe a design I've come up with for a mechanically simple dish mount, which allows rotation anywhere in the sky. Previous mounts needed complicated mechanical parts, and they needed to be very strong. The software needed to run those other mounts was very simple. My design removes the need for any mechanical complexity, instead having the complexity in the software. The design consists of cables coming from three points outside of the dish, each having their length adjustable by winches. This design could be made very portable, fitting in a car or maybe even a backpack for a relatively small dish. Using generic inverse problem solutions twice to create the control algorithm, I can calculate the length of the three cables to point wherever I want in the sky. A graph of the results from the control algorithm can be seen in Figure~\ref{fig:results}. The prototype I've made as a proof-of-concept is shown in Figures~\ref{fig:elec}~and~\ref{fig:phys}.
\end{abstract}

\section{Introduction}
Quite recently I’ve been playing around with listening to images from weather satellites using an omnidirectional antenna optimized for the frequency I’m listening to, 137 MHz. This antenna is designed to only listen upwards, which is ideal for satellites. The disadvantage for this is that it can only listen to relatively powerful signals from low-orbit satellites. These signals, because they’re at a low frequency and have low bandwidth, can’t transmit as much data as one in a higher frequency with higher bandwidth. The satellite signals at higher frequencies are often much harder to receive because they’re less powerful. In order to listen to these signals a high-gain antenna is needed. A simple way of making a high-gain antenna is putting the receiver at the focus of a reflective parabolic dish. These dishes effectively gather more radio waves from the satellite, and focus them to a point, where you put the antenna. However, because the satellites I want to listen to move, it’s necessary to have a way to point the dish to a specific place in the sky, and track the satellite. The standard way to do this is to have two motors at the bottom of the dish, one for each axis. This works very well with strong motors, and a lot of power, so this is what they use on telescopes such as the VLA\cite{napier1983very} in Figure~\ref{fig:vla}. Each of the 27 dishes used in the VLA telescope are 25m in diameter, weigh 209 tons and are supported using a massive mount like the one mentioned earlier. I am  aiming for a simpler, lighter design, one requiring less mechanical strength. So I have come up with a lightweight design to mount a dish up to several meters in diameter and allowing movement in all axes.
\begin{figure}
  \includegraphics[width=\linewidth]{VLA.jpg}
    \hspace*{15pt}\hbox{\scriptsize Credit:\thinspace{\small\itshape Wikimedia user 1Veertje}}

    \hspace*{15pt}\hbox{\scriptsize License:\thinspace{\scriptsize\itshape Creative Commons Attribution 2.0 Generic License}}

  \caption{The Karl G. Jansky Very Large Array in New Mexico, USA.}
  \label{fig:vla}
\end{figure}

\section{Mechanical design}

\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{dish.png}
    \end{center}
    \caption{Simplified 3D render of one way of holding up the dish}
    \label{fig:dishrender}
\end{figure}

The design I've come up with involves 3 poles, placed anywhere (however an equilateral triangle is most efficient). A cable goes between the top of each pole and one of three points around the rim of the dish. The length of these cables will be adjusted by a winch to allow pointing anywhere. As long as the dish can be contained within the triangle formed by connecting these three points and that the points are sufficiently high above the ground to allow the dish to point at the horizon (that is, that the points' height is greater than the diameter of the dish). A simple render of this is shown in Figure~\ref{fig:dishrender}. This could be very portable, assuming the poles can fold down, and are guyed to the ground, rather than permanently mounted.

\subsection{Dish Design}

For an appropriate dish (that is also portable), I have been experimenting with making one out of paper, that folds into the correct shape. If I can make the right template, I should be able to make it out of conductive cloth, which will reflect radio waves towards the recieving antenna. Because this isn't completely rigid, it might not be particularly accurate (close to a perfect paraboloid), but the necessary accuracy of your dish increases with the frequency you're listening to. At my frequencies, the dish should be $\pm 5$cm ($\frac{1}{4}\lambda$, where $\lambda$ is the wavelength)from a perfect paraboloid. This should be achievable.

If the dish is composed of just fabric, it could fold to something very small. The bracing could be something like tent poles, where they just click together. Because both of these items are very light weight, the winches needed to lift it wouldn't need to be very strong. For a 3m dish (capable of not just listening to satellites, but pulsars too), I have a maximum estimate of about 5kg total weight. This is very lightweight compared to the steel of the commercial dishes. One disadvantage of using fabric and a hanging dish is that it wouldn't be able to withstand the wind, however if I were to use a mesh with larger gaps than the fabric, it would reduce windage and hopefully allow it to be out in more wind. But if it does get too windy, this system should be very easy to take down.

A necessary design consideration for the dish is that the centre of gravity be placed in the same plane as $d_{1}$, $d_{2}$, and $d_{3}$. For most dishes, the centre of gravity is on lower side, away from the focus. To counteract this, a counterweight could be placed at the focus, where there will already be the structure necessary to hold the actual antenna.

\begin{figure}
     \begin{center}
        \includegraphics[width=0.8\linewidth]{sample_paraboloid}
    \end{center}
    \caption{One of my generated paraboloids, with 16 segments. This one, when folded up, will be close to the paraboloid of $y=\frac{x^{2}}{4}$, rotated about the $y$ axis to form a 3D surface. The diameter of this one is $8$ units. The number of segments can be increased to have a more accurate surface where necessary.}
    \label{fig:paraboloid}
\end{figure}


\subsection{Electrical Design}

This part of the design is pretty simple, just consisting of three motors and a controller. The controller has a few simple functions:
\begin{itemize}
 \item Listen to a computer over some serial link (I use USB).
 \item Compute how far the motors should rotate given the lengths the computer sent
 \item Be able to home the system, allowing a reference point for later movements.
\end{itemize}
There is already free, open-source software for microcontrollers (one such microcontroller is an Arduino Mega) that fulfills all of these things, such as Marlin, used for 3D printers. This Arduino will be listening for G-code. G-code is a code used for a variety of CNC machines, and basically says 'Move X to here, Y to here, Z to here' and is sent sequentially to tell the machine to follow a predefined pattern to produce a part. If I assign each axis (X, Y, or Z) to one of the motors, and thereby one of the cables, I can have the computer easily control the length of the cables by the XYZ position given to the Arduino. A small issue arises for the last quality of the controller, homing. This requires what are called 'limit switches' they're basically sprung switches that require little force to actuate. These switches are placed at the 'home point' of an axis on a CNC machine. The computer will drive that axis toward the limit switch, until it is pressed. This gives it the home point. For my design, it is necessary to have a way of finding that home, and pressing the switches at that time. The solution I've come up with for this is placing the limit switches at the groove that the cables go through before they go to the dish. A stopper of some sort would then be attached to the cable. That stopper would be able to press the limit switch, indicating the home position.

The motors I'm using are stepper motors. These are accurate motors, that rotate in small steps, something like 1$^{\circ}$ at a time. These motors will have spools attached, where the cable will be kept. If the diameter of this spool is known, and it is long enough to allow the cable to go along its full width without coiling on top of itself, the length of the wire would be very accurate. These motors should be able to provide more than enough force to lift a dish, especially if the load is shared between three of them. In order to drive these motors, it requires a special driver. These drivers are commercially available, and can be used in conjunction with what's called a RAMPS 1.4 board. This board is designed for 3D printers, and has the ability to hold 4 stepper motor drivers (X, Y, Z, extruder), and plugs directly onto an Arduino Mega. Using the software I mentioned earlier (Marlin), these are very easy to control.

\begin{figure}
    \begin{center}
        \includegraphics[width=0.5\linewidth]{system_diagram}
    \end{center}
    \caption{The electrical system diagram.}
    \label{fig:sysdiagram}
\end{figure}

\subsection{Prototype}
For a small-scale prototype, using a subsitute dish 500mm in diameter (a circle), I decided to make a triangular support frame, where the cables that hold up the dish are fishing line, going through grooves cut in wood halfway along each side. The triangle is raised up 500mm, and strengthened with cross bracing.

\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{annotated_electronics}
    \end{center}
    \caption{The electronics used in the prototype. These same motors and controller should be able to drive a 3m dish.}
    \label{fig:elec}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{annotated_prototype}
    \end{center}
    \caption{The mounting for the cables used in the protoype. Not the most efficient design in terms of material usage, but it's quite strong, more than enough to lift the 500mm diameter substitute dish.}
    \label{fig:phys}
\end{figure}

\section{Control Algorithm}

The algorithm I’ve designed is about as generic as I can make it. The points I will be referring to are described in Figure~\ref{fig:dish1}. Given XYZ coordinates for each of the points $p_{1}$, $p_{2}$, and $p_{3}$ as well as the diameter of the dish, it can calculate the lengths of the cables required to point anywhere in the sky. I use a technique of solving inverse problems to do this because of the limitations of both my knowledge of physics and the system I'm controlling.

\begin{figure}
    \begin{center}
        \includegraphics[width=0.3\linewidth]{robot}
    \end{center}
    \caption{A simple two-armed robot. $p_{1}$ is fixed.}
    \label{fig:robot}
\end{figure}

\subsection{Example of an Inverse Problem}
A classic example of this type of algorithm is a two-armed robot in two dimensions, described in Figure~\ref{fig:robot}. The pivot points, $p_{1}$ and $p_{2}$, both are driven by motors that adjust the angles ($\theta_{1}$ and $\theta_{2}$) of the arms $l_{1}$ and $l_{2}$ respectively. The goal of the algorithm is to be able to move the end of the arm, $p_{3}$, to the desired position, $p_{target}$. This is done by adjusting the values of $\theta_{1}$ and $\theta_{2}$. In order to do this, first there needs to be a way of calculating the position of $p_{3}$ if $\theta_{1}$ and $\theta_{2}$ are known. This is called solving the {\em forward problem}, and finding out which $\theta$ values will produce the desired position is called the {\em inverse problem}.

In this example, the forward problem can be done analytically, where...
 \[ p_{3} = \Big(l_{1}sin(\theta_{1}) + l_{2}sin(\theta_{2}),  l_{1}cos(\theta_{1}) + l_{2}cos(\theta_{2}) \Big) \]
With this solution to the forward problem, I can calculate a score for that position of $p_{3}$, and because $l_{1}$ and $l_{2}$ are fixed, the score of that position depends only on $\theta_{1}$ and $\theta_{2}$. This will be important later. The score for the position of $p_{3}$ is dependent on the distance to $p_{target}$. The smaller the distance or score, the better the position.

So if I write a function that takes in $\theta_{1}$ and $\theta_{2}$, and returns the distance between $p_{3}$ and $p_{target}$, I can use a minimiser on that function to find the optimum values of $\theta_{1}$ and $\theta_{2}$, where $p_{3}$ is as close as possible to $p_{target}$. A minimiser is a function that finds these optimal values. It is given a function (such as the function I just wrote) which returns a score, and tries to find whatever input values to the given function have the smallest output, or score. And there you go! Now you have solved the inverse problem. If you were to run that minimiser with different $p_{target}$s and you can move it however you like.

This is a very versatile way of solving many different problems, and when you don't solve it analytically, you may find that things you thought wouldn't be adjustable are.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{dish_diagram}
    \caption{Top down diagram of the rotator}
    \label{fig:dish1}
\end{figure}

\subsection{Forward Problem for Dish Pointing}

This function, when given the length of the three cables ($l_{1}$, $l_{2}$, and $l_{3}$), will calculate the position and rotation of the dish. This can’t be analytically solved simply like the previous example, so I have to use an inverse solution to the forward part of the bigger problem. In order to do this, I created a function which, when given guesses for the points $d_{1}$, $d_{2}$, and $d_{3}$, calculates the values relating to the position and size of the dish, then calculates a score for those points. This score can be represented by this set of equations:

$$ S_{x} = (x_{1} - l_{t})^{2} + (x_{2} - l_{t})^{2} + (x_{3} - l_{t})^{2} $$

This part determines how far the points $d_{1}$, $d_{2}$, and $d_{3}$ are from forming an equilateral triangle in 3D space, where $l_{t} = \sqrt{3} \times \frac{D}{2}$ where $D$ is the diameter of the dish. This is the ideal length of $x_{1}$.

$$ S_{l} = (l_{1} - l_{1d}) + (l_{2} - l_{2d}) + (l_{3} - l_{3d}) $$


This part determines how far $l_{1}$, $l_{2}$, and $l_{3}$ are from the correct length, where $l_{1d}$ is the desired length of $l_{1}$.

$$ S_{E_{p}} = \left(\frac{d_{1z} + d_{2z} + d_{1z}}{3}\right)^{2} $$

This part calculates the average potential energy of the dish. This is done in order to prevent the dish from being inverted. It effectively simulates gravity, forcing the minimiser to find the lowest possible dish position that fits all the other criteria. In this equation, $d_{1z}$ is the Z component of $d_{1}$.

$$ S = S_{x} + S_{l} + S_{E_{p}} $$

Now that I have a score based on a guess of $d_{1}$, $d_{2}$, and $d_{3}$, I can minimise the score using a minimiser similar to the one I described earlier (solving the inverse problem of the forward problem).
As this code is written in a programming language called Python, I can use SciPy\footnote{SciPy is a library of code for Python which does a variety of computations useful for science (mostly physics). It has many parts, but I'm using the 'optimisation' module. I also used SciPy for the vector math (from the 'linear algebra' module), as it has fast ways of doing those things. Later I use 'matplotlib' to plot the results.} \cite{jones2001scipy}, which has a variety of inbuilt optimisation algorithms (I ended up using 'L-BFGS-B') to find the position of the dish, given $l_{1}$, $l_{2}$, and $l_{3}$. This operation takes very little time on a modern computer (23ms).

\subsection{Inverse Problem}


This function is given a position in the sky that the user wants the dish to point to. It minimizes a scoring function in order to figure out the length of $l_{1}$, $l_{2}$, and $l_{3}$ required to point there. This is the main functionality of the algorithm. Rather than starting with a random length of $l_{1}$, $l_{2}$, and $l_{3}$, it uses the current lengths as a starting point in order to minimise time, as it is rare that it'll have to compute a position halfway across the sky if it's just tracking something that's moving relatively slowly. The lengths are scored by this set of equations:

$$S_{pointing} = \left| \vec{a}_{desired} - \vec{a}_{current} \right|^{2}$$

This returns the length of the vector separating the current vector ($\vec{a}_{current}$) and the desired vector ($\vec{a}_{desired}$). This represents the pointing error, hence $S_{pointing}$. This is how far the dish is from pointing in the right direction. This is squared in order to value it more, as well as to punish large errors more than small ones.

$$S_{d} = \frac{(l_{1} - l_{1prev})^{2} + (l_{2} - l_{2prev})^{2} + (l_{3} - l_{3prev})^{2}}{10000} $$
How far each of these lengths are from their previous length. Each of the individual differences is squared to do two things. First, if it's negative, it will become positive. Second, it means that big differences matter more than little differennces. This is divided by 10000 because I value the angular pointing more, and don't want to detract from that.

$$S = S_{pointing} + S_{d} + S_{special}$$

If the dish goes below the ground, I set $S_{special}$ to be $10^{6}$, effectively disqualifying these lengths, otherwise $S_{special} = 0$. I check if the dish is below the ground by seeing if the Z component of any of $d_{1}$, $d_{2}$, or $d_{3}$ is $< 0$.

Again this score is minimised using SciPy’s inbuilt module, however this time I used the 'Nelder-Mead'\cite{nelder1965simplex} method. The time it takes to do this operation depends on how far you’re aiming to go from your current position (as it uses the current position as a starting point), but on average it takes a couple of seconds. I've set the maximum number of iterations (attempts at the smallest score) to 500, so the maximum time it would take is 23ms$\times 500$ iterations, or 11.5s total.

\subsection{Other Optimizations}

Although this algorithm can compute a new, nearby, set of lengths in a short time, I decided to try and optimise it further. One of these optimsations was what's called caching the result. It basically is saving the result in a database. So once you've specified the dimensions of your system (the positions of $p_{1}$, $p_{2}$, and $p_{3}$, and the diameter of the dish, $D$), every position in the sky will be assigned a set of three lengths, the lengths of the cables. If the position you want to go to is nearby a previously saved position, you could just get those lengths without having to compute it every time.

A possible future optimisation is using a different programming language. Python isn't a very fast language for many reasons, but it's very easy to read and write. Switching to another language, like Rust or similar, should give a large speed up, however the issue with switching to languages like that is that they don't always have as good libraries for scientific computation. Python has been around long enough to have gained a good following of people in the community who have contributed many excellent libraries (SciPy is a good example of an excellent library written by this community).

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\linewidth]{algorithm80}
        \caption{80$^{\circ}$ above the horizon}
        \label{fig:algorithm80}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\linewidth]{algorithm40}
        \caption{40$^{\circ}$ above the horizon.}
        \label{fig:algorithm40}
    \end{subfigure}
    \caption{The results, plotted in matplotlib, part of SciPy. The purple triangle represents the dish.}
    \label{fig:results}
\end{figure}

\section{Integration into tracking software}

While I can use this software as is, manually entering in positions, it is optimal to use some tracking software. There are many pieces of software for doing these sorts of orbital calculations, but I've grown used to one called Gpredict, which also does the calculations for doppler shift and allows control of my favourite radio control program, GQRX. The way it communicates with external programs (like the one I'm aiming to write) is through an internet TCP socket. Assuming I can create a TCP socket that can listen to what Gpredict is saying, I should be able to tell where to point for any satellite. Using the Python package called 'socket', it's about 30 lines of code to open one up and parse the messages.

\section{The Code}
The code is available on Gitlab at:
\begin{center}
\url{http://gitlab.com/Ecotiny/hanging-dish-rotator}.
\end{center}
It is open source (under GPLv3), and I will hopefully switch from Python to Rust at some point.

\bibliographystyle{plain}
\bibliography{writeup}
\end{document}
