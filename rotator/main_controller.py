import numpy as np
import math
import random
from scipy.optimize import minimize
import sqlite3
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib
import scipy
import time
class Dish:
    def __init__(self, pvt1, pvt2, pvt3, diameter, resolution):
        self.pvt1 = pvt1
        self.pvt2 = pvt2
        self.pvt3 = pvt3
        self.elaz = [90,0]
        #self.cachefile = cachefile
        self.resolution = resolution

        self.current_x = 0
        self.cur_vect = self.asCartesian([1.0, 90-self.elaz[0], self.elaz[1]])

        self.conn = sqlite3.connect('cache.db')
        self.c = self.conn.cursor()

        # Create table
        self.c.execute('''CREATE TABLE IF NOT EXISTS vals
                    (x real, y real, z real, elevation real, azimuth real, l1 real, l2 real, l3 real)''')


        '''
        pivots should be arranged as so in equilateral triangle

                   |
                  pv1
                 / | \
                /  |  \
         ------/---+---\------
              /    |    \
             pv3--------pv2
                   |
                   |

        '''

        self.diameter = diameter
        self.tri_length = math.sqrt(3) * (diameter/2)
        self.current_lengths = [10,10,10]      # Previous Lengths



    def x_val_score(self, x, lengths):
        x1,y1,z1,x2,y2,z2,x3,y3,z3 = x
        l1,l2,l3 = lengths

        p1 = np.array([x1, y1, z1])
        p2 = np.array([x2, y2, z2])
        p3 = np.array([x3, y3, z3])

        # two vectors in plane
        v1 = p3 - p1 #np.subtract([x3,y3,z3], [x1,y1,z1])
        v2 = p2 - p1 # np.subtract([x2,y2,z2], [x1,y1,z1])

        normal_vector = np.cross(v1,v2)

        z = normal_vector[2]

        #distance check of dish points (make sure they are tri length apart)
        dist1 = np.linalg.norm(p1 - p2) #distance(x1,y1,z1,x2,y2,z2)
        dist2 = np.linalg.norm(p1 - p3) #distance(x1,y1,z1,x3,y3,z3)
        dist3 = np.linalg.norm(p2 - p3) #distance(x2,y2,z2,x3,y3,z3)

        #distance check to pivot points as vectors for speed
        dist1_to_p1 = np.linalg.norm(p1 - self.pvt1) #distance(x1,y1,z1,pivots[0,0],pivots[0,1],gantry_height)
        dist2_to_p2 = np.linalg.norm(p2 - self.pvt2) # distance(x2,y2,z2,pivots[1,0],pivots[1,1],gantry_height)
        dist3_to_p3 = np.linalg.norm(p3 - self.pvt3) #distance(x3,y3,z3,pivots[2,0],pivots[2,1],gantry_height)


        edge_lengths = (dist1 - self.tri_length)**2 + (dist2 - self.tri_length)**2 + (dist3 - self.tri_length)**2 # lengths of the edge of "triangle" formed by x points

        potential_energy = ((z1 + z2 + z3)/3)**2 # average potential energy of all 3 points

        string_lengths = (dist1_to_p1 - l1)**2 + (dist2_to_p2 - l2)**2 + (dist3_to_p3 - l3)**2 # check how close our current string lengths are to what we want

        return edge_lengths + potential_energy + string_lengths


    def get_x(self, lengths, x_start=None):

        l1,l2,l3 = lengths

        if x_start is None:
            x1 = random.uniform(pivots[2][0], pivots[1][0])
            x2 = random.uniform(pivots[2][0], pivots[1][0])
            x3 = random.uniform(pivots[2][0], pivots[1][0])
            y1 = random.uniform(pivots[2][1], pivots[0][1])
            y2 = random.uniform(pivots[2][1], pivots[0][1])
            y3 = random.uniform(pivots[2][1], pivots[0][1])
            z1 = random.uniform(0, gantry_height)
            z2 = random.uniform(0, gantry_height)
            z3 = random.uniform(0, gantry_height)
            x_start = [x1, y1, z1, x2, y2, z2, x3, y3, z3]

        d = minimize(self.x_val_score, x_start, method="L-BFGS-B", options={'maxiter': 500},args=(lengths,))
        return d.x

    def asCartesian(self, rthetaphi):
        #takes list rthetaphi (single coord), returns xyz of cartesian coord system
        r       = rthetaphi[0]
        theta   = np.radians(rthetaphi[1])
        phi     = np.radians(rthetaphi[2])

        x = r * math.sin( theta ) * math.cos( phi )
        y = r * math.sin( theta ) * math.sin( phi )
        z = r * math.cos( theta )

        length = scipy.linalg.norm([x,y,z])

        assert length > 0.99 and length < 1.01

        return np.array([x,y,z])

    def asSpherical(self,xyz):
        #                                      len ele  azi
        #takes list xyz (single coord), returns r theta phi of spherical coord system
        x       = xyz[0]
        y       = xyz[1]
        z       = xyz[2]
        r       =  math.sqrt(x*x + y*y + z*z)
        theta   =  np.degrees(math.acos(z/r))
        phi     =  np.degrees(math.atan2(y,x))
        return [r,theta,phi]

    def get_theta(self, vect_a, vect_b):      # Difference in angle between two unit vectors

        dp = np.dot(vect_a, vect_b)
        theta = np.arccos(dp)

        return np.degrees(theta)

    def get_norm(self, x):                    # Returns normal vector as np array
        x1,y1,z1,x2,y2,z2,x3,y3,z3 = x

        v1 = np.array([x1, y1, z1])
        v2 = np.array([x2, y2, z2])
        v3 = np.array([x3, y3, z3])

        v13 = v3 - v1
        v12 = v2 - v1

        normal_vector = np.cross(v13,v12)

        return normal_vector

    def normalise(self, vect):
        length = scipy.linalg.norm(vect)

        return vect / length

    def internal_get_elaz(self, x):
        x1,y1,z1,x2,y2,z2,x3,y3,z3 = x

        # two vectors in plane
        normal_vector = self.get_norm(x)

        r, theta, phi = self.asSpherical(normal_vector)

        return [theta, phi]

    def score_lengths(self, guess_l, prev_l, desi_vect, x_start):
        guess_l1, guess_l2, guess_l3 = guess_l
        prev_l1, prev_l2, prev_l3 = prev_l

        x = self.get_x(guess_l, x_start)

        cur_vect = self.normalise(self.get_norm(x))

        dl1 = (prev_l1 - guess_l1)**2
        dl2 = (prev_l2 - guess_l2)**2
        dl3 = (prev_l2 - guess_l2)**2

        d_azel = (self.get_theta(desi_vect,cur_vect))**2

        special = 0 # for punishment

        if min(guess_l1,guess_l2,guess_l3) < 0:
            special += 1e6

        if cur_vect[2] < 0:
            special += 1e9

        total_diff = d_azel + ((dl1 + dl2 + dl3)/10000) + special # devalue difference because we value angle more

        return total_diff

    def calculate_lengths(self):
        # Actually do the calculation of what the lengths should be given the desired point

        l_start = np.array(self.current_lengths)
        prev_l = np.array(self.current_lengths)

        x_start = self.get_x(prev_l, None)

        d = minimize(self.score_lengths, l_start, method = 'Nelder-Mead', args=(prev_l, self.desi_vect, x_start), options={"maxiter": 300})

        return d.x # Returns array containing three legths

    def get_lengths(self): # Gets current lengths
        return self.current_lengths

    def vect_plot(self, ax, v1, color="red"): #plots vector on ax
        ax.quiver(0,0,0, v1[0], v1[1], v1[2], color=color)

    def external_get_elaz(self, x): # get elaz from outside this class
        x1,y1,z1,x2,y2,z2,x3,y3,z3 = x

        # two vectors in plane
        normal_vector = self.get_norm(x)

        r, theta, phi = self.asSpherical(normal_vector)

        return [90-theta, phi]

    def plot(self): #plot current dish position

        #CHANGE DEFAULT COLORS
        colors = ["red", "blue", "green", "purple"]

        l1,l2,l3 = self.current_lengths
        x1,y1,z1,x2,y2,z2,x3,y3,z3 = self.current_x

        v1 = np.array([x1, y1, z1])
        v2 = np.array([x2, y2, z2])
        v3 = np.array([x3, y3, z3])

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(pivots[0,0], pivots[0,1], pivots[0,2], color=colors[0])
        ax.scatter(pivots[1,0], pivots[1,1], pivots[1,2], color=colors[1])
        ax.scatter(pivots[2,0], pivots[2,1], pivots[2,2], color=colors[2])

        #ax.set_xlim(-300,300)
        #ax.set_ylim(-300,300)
        #ax.set_zlim(0,650)

        dish_x_vals = np.array([x1,x2,x3,x1]) # last entry for connecting of triangle
        dish_y_vals = np.array([y1,y2,y3,y1])
        dish_z_vals = np.array([z1,z2,z3,z1])

        # normal vector
        centroid = (v1 + v2 + v3)/3

        v13 = v3 - v1
        v12 = v2 - v1

        normal_vector = np.cross(v13,v12)/500.0

        print(("normal vector bf: {}".format(normal_vector)))

        #self.vect_plot(ax, normal_vector, color="red")
        #self.vect_plot(ax, v1, color="red")
        #self.vect_plot(ax, v2, color="blue")
        #self.vect_plot(ax, v3, color="green")
        #self.vect_plot(ax, v12, color="red")
        #self.vect_plot(ax, v13, color="red")


        ax.plot(dish_x_vals, dish_y_vals, dish_z_vals, color=colors[3])
        # string
        ax.plot([x1, pivots[0,0]], [y1, pivots[0,1]], [z1, pivots[0,2]], color=colors[0]) # l1
        ax.plot([x2, pivots[1,0]], [y2, pivots[1,1]], [z2, pivots[1,2]], color=colors[1]) # l2
        ax.plot([x3, pivots[2,0]], [y3, pivots[2,1]], [z3, pivots[2,2]], color=colors[2]) # l2

        #legend
        scatter1_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[0], marker = 'o')
        scatter2_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[1], marker = 'o')
        scatter3_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[2], marker = 'o')
        ax.legend([scatter1_proxy, scatter2_proxy, scatter3_proxy], ['p1/l1'.format(l1), 'p2/l2'.format(l2), 'p3/l3'.format(l3)], numpoints = 1)

        a = (self.diameter +100) / 2
        ax.auto_scale_xyz([-a,a],[-a,a],[0,2*a])

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        plt.show()

    def point(self, elev, azimuth):
        self.desi_vect = self.asCartesian([1.0,90-elev,azimuth])


        #if elev < 45:
            #fig = plt.figure()
            #ax = fig.add_subplot(111, projection='3d')

            #self.vect_plot(ax, self.desi_vect)

            #a = 5
            #ax.auto_scale_xyz([-a,a],[-a,a],[-a,a])

            #ax.set_xlabel('x')
            #ax.set_ylabel('y')
            #ax.set_zlabel('z')

            #plt.show()

        self.desi_vect = self.normalise(self.desi_vect)
        #print "========"
        #print "desi_vect: {}".format((self.desi_vect[0], self.desi_vect[1], self.desi_vect[2]))

        # Check for cache, if it is there do the right thing.
        # For the moment, just call calculate_lengths

        l1, l2, l3 = (0,0,0)

        r = self.resolution/2
        got_from_db = False

        query = "SELECT * FROM vals WHERE (x BETWEEN {} AND {}) AND (y BETWEEN {} AND {}) AND (z BETWEEN {} AND {})".format(
                                       self.desi_vect[0]-r, self.desi_vect[0]+r,
                                       self.desi_vect[1]-r, self.desi_vect[1]+r,
                                       self.desi_vect[2]-r, self.desi_vect[2]+r)

        #print query

        prev_vals = self.conn.execute(query)
        results = prev_vals.fetchall()

        if (len(results) > 0):
            #                   0  1  2  3   4   5   6   7
            # nextone in format x, y, z, el, az, l1, l2, l3
            nextone = results[0]
            l1, l2, l3 = [nextone[5], nextone[6], nextone[7]]
            self.cur_vect = np.array([nextone[0], nextone[1], nextone[2]])
            self.elaz = [nextone[3], nextone[4]]
            print("getting vals from database")
            got_from_db = True

        if not got_from_db:
            l1, l2, l3 = self.calculate_lengths()

            # Now return stuff.
            self.current_x = self.get_x(np.array([l1,l2,l3]), None)
            self.elaz = self.external_get_elaz(self.current_x)

            self.cur_vect = self.asCartesian([1.0, 90-self.elaz[0], self.elaz[1]])
            self.cur_vect = self.normalise(self.cur_vect)

            self.c.execute("INSERT INTO vals VALUES (?,?,?,?,?,?,?,?)", (self.cur_vect[0], self.cur_vect[1], self.cur_vect[2], self.elaz[0], self.elaz[1], l1, l2, l3))
            self.conn.commit()

            error = self.get_theta(self.desi_vect, self.cur_vect)

        self.current_lengths = np.array([l1, l2, l3])

        #self.plot()

        return [l1, l2, l3]

    def slew(self, el, az):

        self.desi_vect = self.asCartesian([1.0,90-el,az])
        self.desi_vect = self.normalise(self.desi_vect)
        difference = self.get_theta(self.desi_vect, self.cur_vect)

        if difference > 3:
            el_vals = np.arange(min(self.elaz[0], el), max(self.elaz[0], el), 1)
            az_vals = np.linspace(min(self.elaz[1], az), max(self.elaz[1], az), len(el_vals))

            both_vals = np.dstack((el_vals, az_vals))[0] # for some reason creates array with one array inside it

            l1,l2,l3 = [0,0,0]

            for elaz in both_vals:
                el, az = elaz
                l1,l2,l3 = self.point(el, az)

            return [l1,l2,l3]



def function_name(d, el, az, counter, tolerance):
    counter += 1
    d.slew(el, az)
    actual = d.asCartesian([1,90-d.elaz[0],d.elaz[1]])
    desired = d.asCartesian([1,90-el,az])

    error = d.get_theta(actual, desired)

    if error > tolerance:
        print("WOAH WOAH WOAH, error of: {}".format(error))
        print("wanted elaz: {}".format((el,az)))
        print("actual elaz: {}".format(d.elaz))
        print("count: {}".format(counter))
        print("===============")
    else:
        print("Count: {}".format(counter))
        #print "Good at {}, {}".format(d.elaz, counter)
        #print "Error: {}".format(error)
    return counter

def test_run(d, tolerance):
    reverse = True

    counter = 0

    for az in np.linspace(-180, 180, 30):
        if reverse:
            for el in np.linspace(5.0, 85.0, 30)[::-1]:
                counter = function_name(d, el, az, counter, tolerance)
            reverse = False
        else:
            for el in np.linspace(5.0, 85.0, 30):
                counter = function_name(d, el, az, counter, tolerance)
            reverse = True

if __name__=="__main__":

    dish_radi = 250
    gantry_height = 600

    # calculate pivot points
    pivots = np.array([[0,0,gantry_height],
                    [0,0,gantry_height],
                    [0,0,gantry_height]])

    tri_length = math.sqrt(3) * dish_radi
    x_offset = tri_length/2
    y_offset = (math.sqrt(3)/6) * tri_length

    # x1,y1
    pivots[0,0] = 0
    pivots[0,1] = dish_radi
    # x2,y2
    pivots[1,0] = x_offset
    pivots[1,1] = y_offset * -1
    # x3,y3
    pivots[2,0] = x_offset * -1 # below 0
    pivots[2,1] = y_offset * -1

    p1 = np.array([pivots[0,0], pivots[0,1], gantry_height])
    p2 = np.array([pivots[1,0], pivots[1,1], gantry_height])
    p3 = np.array([pivots[2,0], pivots[2,1], gantry_height])

    d = Dish(p1, p2, p3, dish_radi*2, 0.01)

    tolerance = 1.5 # degrees

    for el in range(90,30,-10):
        start = time.time()
        d.point(el,0)
        end = time.time()
        print(el, end-start)
    print("Pointed")

    d.plot()

    #test_run(d, tolerance)


