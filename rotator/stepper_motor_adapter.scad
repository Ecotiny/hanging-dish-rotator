outer_radius = 5.5;
shaft_radius = 2.5;
shaft_length = 20;
setscrew_diam = 2.5;
setscrew_height = 10;
$fn = 20;

rotate([0,180,0])difference() {
    cylinder(r=outer_radius, h = shaft_length + 7);
    translate([0,0,-1])cylinder(r = shaft_radius+0.1, h = shaft_length + 3);
    translate([0,0,setscrew_height])rotate([0,90,0])cylinder(d=setscrew_diam, h=outer_radius+2);
}